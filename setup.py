# -*- coding: utf-8 -*-
from setuptools import setup
from os import getcwd, path

if not path.dirname(__file__):  # setup.py without /path/to/
    _dirname = getcwd()  # /path/to/
else:
    _dirname = path.dirname(path.dirname(__file__))


def read(name, default=None, debug=True):
    try:
        filename = path.join(_dirname, name)
        with open(filename) as f:
            return f.read()
    except Exception as e:
        err = "%s: %s" % (type(e), str(e))
        if debug:
            print(err)
        return default


def lines(name):
    txt = read(name)
    return map(
        lambda l: l.lstrip().rstrip(),
        filter(lambda t: not t.startswith('#'), txt.splitlines() if txt else [])
    )

install_requires = [i for i in lines("requirements.txt")]

setup(
    name='key_value_store',
    version='1.0.0',
    author='rohit taneja',
    url='https://gitlab.com/rohit-taneja/key_value_store',
    description='A minimal and fast key value store',
    packages=['key_value_store'],
    install_requires=install_requires
)
