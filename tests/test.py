import hug
from key_value_store.start import get_value

def test_get():
    response = hug.test.get(get_value, '/store/size')
    assert response.status == 200
    assert response.data is not None

