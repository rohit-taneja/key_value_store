import hug

from key_value_store.redis import RedisCache
from key_value_store.constants import Constants
from key_value_store.error_messages import ErrorMessages

config = Constants.config
version = Constants.api_version


RedisCache.connect(config.get('REDIS_HOST', Constants.default_host), config.get('REDIS_PORT', Constants.default_redis_port), max_connections=config.get('REDIS_MAX_CONNECTIONS', Constants.default_redis_max_connections))


@hug.get('/store/key/{key}', versions = version)
def get_value(key: str):
    value = RedisCache.get_key(key)
    if value is None:
        return get_failure_response(ErrorMessages.key_does_not_exist)
    return get_success_response(value)

def _set_key(key: str, value: str):
    RedisCache.set_key(key, value)
    return get_success_response()

@hug.put('/store/key/{key}', versions = version)
def update_value(key: str, value: str):
    if RedisCache.get_key(key) is None:
        return get_failure_response(ErrorMessages.key_does_not_exist)
    return _set_key(key, value)

@hug.post('/store/key/{key}', versions = version)
def create_entry(key: str, value: str):
    if RedisCache.get_key(key) is not None:
        return get_failure_response(ErrorMessages.key_already_exist)
    return _set_key(key, value)


@hug.delete('/store/key/{key}', versions = version)
def delete_entry(key: str):
    result = RedisCache.delete(key)
    if not result:
        return get_failure_response(ErrorMessages.key_does_not_exist)
    return get_success_response()

@hug.delete('/store', versions = version)
def clear_all_keys():
    RedisCache.flush_db()
    return get_success_response()


@hug.get('/store/size', versions = version)
def get_size():
    return get_success_response(RedisCache.size())

def get_success_response(data=Constants.default_success_response):
    return {"result":data}

def get_failure_response(error=ErrorMessages.default_failure_response):
    return {"errors":{"value":error}}