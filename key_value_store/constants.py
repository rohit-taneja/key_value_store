from key_value_store.utils import Utils

class Constants:
    config = Utils.json_file_to_dict('./config.json')
    default_host = '127.0.0.1'
    default_redis_port = '6379'
    default_redis_max_connections = 5
    cache_namespace = 'store'
    default_success_response = 'Success'
    api_version = 1