import json

class Utils:

    @classmethod
    def json_file_to_dict(cls, file: str) -> dict:
        with open(file) as config_file:
            config = json.load(config_file)

        return config