from threading import Lock
import redis

class RedisCache:
    _redis = None
    _host = None
    _port = None
    _lock = Lock()
    _max_connections = None

    @classmethod
    def get_redis(cls):
        if not cls._redis:
            with cls._lock:
                if not cls._redis:
                    pool = redis.ConnectionPool(host='localhost', port=6379, db=1, max_connections=cls._max_connections)
                    cls._redis = redis.Redis(connection_pool=pool)
        return cls._redis

    @classmethod
    def connect(cls, host, port, max_connections=10):
        cls._host = host
        cls._port = port
        cls._max_connections = max_connections

    @classmethod
    def set_key(cls, key, value, expire=None):
        redis_pool = cls.get_redis()
        redis_pool.set(key, value, ex=expire)

    @classmethod
    def get_key(cls, key):
        redis_pool = cls.get_redis()
        result = redis_pool.get(key)
        return cls.decode(result)
    
    @classmethod
    def decode(cls, data : bytes):
        if data is None:
            return data
        return data.decode()

    @classmethod
    def delete(cls, key):
        redis_pool = cls.get_redis()
        return redis_pool.delete(key)

    @classmethod
    def flush_db(cls):
        redis_pool = cls.get_redis()
        redis_pool.flushdb()

    @classmethod
    def size(cls):
        redis_pool = cls.get_redis()
        return redis_pool.dbsize()
